/**
 * Story model
 */


module.exports = function(sequelize, DataTypes) {
    var Story = sequelize.define('Story', {
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        nextChapterNumber: {
            type: DataTypes.INTEGER,
            defaultValue: 1
        },
        chapterCount: {
            type: DataTypes.INTEGER,
            defaultValue: 1
        },
        estimatedWordCount: {
            type: DataTypes.INTEGER
        }
    },{
        classMethods: {
            associate: function(models) {
                Story.belongsTo(models.User, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
                Story.belongsToMany(models.Tag, {through: 'StoryTag'});
                Story.hasMany(models.StoryTag);
                Story.hasMany(models.Chapter);
                Story.hasMany(models.Comment);
                Story.hasMany(models.StoryFavorite);
            }
        },
        instanceMethods: {
            getDateUpdatedAt: function() {
                return new Date(this.updatedAt).toDateString();
            }
        }
    });
    return Story;
};


