/**
 * Created by hschmale on 5/16/16.
 */

module.exports = function(sequelize, DataTypes) {
    var StoryFavorite = sequelize.define('StoryFavorite', {
    },{
        classMethods: {
            associate: function(models) {
                StoryFavorite.belongsTo(models.User);
                StoryFavorite.belongsTo(models.Story);
            }
        },
        indexes: [
            {
                unique: true,
                fields: ['UserId', 'StoryId']
            }
        ],
        updatedAt: false
    });
    return StoryFavorite;
};


