'use strict';

module.exports = function (sequelize, DataTypes) {
    var NewsItem = sequelize.define('NewsItem', {
        title: DataTypes.STRING,
        text: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                NewsItem.belongsTo(models.User);
            }
        },
        instanceMethods: {
            getPreview: function(numWords){
                return this.text.split(/\s+/).splice(0, numWords).join(' ');
            }
        },
        updatedAt: false
    });
    return NewsItem;
};