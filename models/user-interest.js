/**
 * Tracks user interests
 *
 * Created by hschmale on 5/20/16.
 */

module.exports = function(sequelize, DataTypes) {
    var UserInterest = sequelize.define('UserInterest', {
        weight: DataTypes.INTEGER
    },{
        classMethods: {
            associate: function(models) {
                UserInterest.belongsTo(models.User);
                UserInterest.belongsTo(models.Tag);
            }
        },
        timestamps: false
    });
    return UserInterest;
};