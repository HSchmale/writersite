/**
 * Created by hschmale on 6/13/16.
 */

module.exports = function (sequelize, DataTypes) {
    var FreqIdx = sequelize.define('FreqIdx', {
        word: {
            type: DataTypes.STRING,
            allowNull: false
        },
        weight: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        StoryId: {
            type: DataTypes.INTEGER,
            references: {
                model: 'Stories',
                key: 'id'
            },
            onUpdate: 'cascade',
            onDelete: 'cascade'
        }
    }, {
        classMethods: {
            associate: function (models) {
                // associations can be defined here
                FreqIdx.belongsTo(models.Story);
            }
        },
        updatedAt: false,
        createdAt: false,
        tableName: 'FreqIdx'
    });
    FreqIdx.removeAttribute('id');
    return FreqIdx;
};