/**
 * Story Tag Model
 *
 * Created by hschmale on 5/16/16.
 */

module.exports = function(sequelize, DataTypes) {
    var StoryTag = sequelize.define('StoryTag', {
        StoryId: DataTypes.INTEGER,
        TagId: DataTypes.INTEGER
    },{
        classMethods: {
            associate: function(models) {
                models.StoryTag.belongsTo(models.Story);
                models.StoryTag.belongsTo(models.Tag);
            }
        },
        timestamps: false
    });
    return StoryTag;
};