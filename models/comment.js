/**
 * Comment model
 *
 * Created by hschmale on 5/15/16.
 */


module.exports = function(sequelize, DataTypes) {
    var Comment = sequelize.define('Comment', {
        text: DataTypes.TEXT,
        ChapterNum: DataTypes.INTEGER
    },{
        classMethods: {
            associate: function(models) {
                Comment.belongsTo(models.User);
                Comment.belongsTo(models.Story);
                Comment.hasMany(models.Notification);
            }
        }
    });
    return Comment;
};