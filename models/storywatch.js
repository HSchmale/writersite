/**
 * Created by hschmale on 5/19/16.
 */

module.exports = function(sequelize, DataTypes) {
    var StoryWatch = sequelize.define('StoryWatch', {
        startWatchChap: DataTypes.INTEGER
    },{
        classMethods: {
            associate: function(models) {
                StoryWatch.belongsTo(models.User);
                StoryWatch.belongsTo(models.Story);
            }
        }
    });
    return StoryWatch;
};
