/**
 * Defines how notifications are stored in the database.
 * Created by hschmale on 5/19/16.
 */

module.exports = function (sequelize, DataTypes) {
    var Notification = sequelize.define('Notification', {
        type: {
            type: DataTypes.INTEGER
        },
        chapNum: {
            type: DataTypes.INTEGER
        },
        msg: {
            type: DataTypes.STRING(512)
        }
    }, {
        classMethods: {
            associate: function (models) {
                Notification.belongsTo(models.User, {
                    as: 'OwnerId'
                });
                Notification.belongsTo(models.Story);
                Notification.belongsTo(models.Chapter);
                Notification.belongsTo(models.Comment);
            }
        },
        instanceMethods: {},
        updatedAt: false
    });
    //*/
    //return null;
    return Notification;
};
