/**
 * User model
 */

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        displayName: DataTypes.STRING,
        realName: DataTypes.STRING,
        email: DataTypes.STRING,
        oauthProvider: DataTypes.STRING,
        oauthUserId: {
            type: DataTypes.STRING
        },
        lastLoggedIn: {
            type: DataTypes.DATE
        },
        accountEnabled: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        oauthToken: DataTypes.STRING,
        oauthRefreshToken: DataTypes.STRING,
        description: DataTypes.TEXT,
        imageUrl: DataTypes.TEXT,
        isAdmin: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isMod: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        isBanned: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    },{
        classMethods: {
            associate: function(models) {
                User.hasMany(models.Story);
                User.hasMany(models.Comment);
                User.hasMany(models.StoryFavorite);
                User.hasMany(models.StoryWatch);
                User.hasMany(models.Notification, {
                    as: 'OwnerId'
                });
                User.hasMany(models.NewsItem);
                User.belongsToMany(models.Tag, {through: models.UserInterest });
            }
        }
    });
    return User;
};


