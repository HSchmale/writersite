/**
 * Created by hschmale on 5/12/16.
 */


module.exports = function (sequelize, DataTypes) {
    var Chapter = sequelize.define('Chapter', {
        title: DataTypes.STRING,
        chapterNum:{
            type: DataTypes.INTEGER,
            defaultValue: 1
        },
        estimatedWordCount: {
            type: DataTypes.INTEGER
        },
        text: DataTypes.TEXT,
        htmlText: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function (models) {
                Chapter.belongsTo(models.Story);
                Chapter.hasMany(models.Notification);
            }
        }
    });
    return Chapter;
};
