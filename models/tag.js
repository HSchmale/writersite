module.exports = function(sequelize, DataTypes) {
    var Tag = sequelize.define('Tag', {
        name: {
            type: DataTypes.STRING,
            unique: true
        },
        description: DataTypes.STRING,
        useCount: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        }
    },{
        classMethods: {
            associate: function(models) {
                Tag.belongsToMany(models.Story, {through: models.StoryTag});
                Tag.belongsToMany(models.User,  {through: models.UserInterest });
            }
        },
        timestamps: false
    });
    return Tag;
};