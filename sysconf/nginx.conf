# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log  /var/log/nginx/access.log  main;

    proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=one:8m max_size=3000m inactive=600m;
    proxy_temp_path /var/tmp;

    gzip on;
    gzip_comp_level 6;
    gzip_vary on;
    gzip_min_length 1000;
    gzip_proxied any;
    gzip_types  text/plain text/html text/css application/json application/x-javascript
                text/xml application/xml application/xml+rss text/javascript application/javascript
                application/font-woff;
    gzip_buffers 16 8k;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    upstream writersite {
        server localhost:3000;
        keepalive 64;
    }

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    # include /etc/nginx/conf.d/*.conf;

    server {
        listen 80 default;
        server_name www.writersite.xyz;
        rewrite    ^ https://$server_name$request_uri? permanent;
    }

    server {
        listen       443 ssl;

        server_name  www.writersite.xyz;


        ssl_certificate /etc/letsencrypt/live/www.writersite.xyz/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/www.writersite.xyz/privkey.pem;
        ssl_protocols SSLv3 TLSv1;
        ssl_ciphers HIGH:!aNULL:!MD5;

        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        # include /etc/nginx/default.d/*.conf;

        # Enable access from the web root for letsencrypt
        location ~ /.well-known {
            allow all;
         }
        
        location ~ ^/(images/|fonts/|static/|robots.txt|humans.txt|favicon.ico) {
            root /opt/writersite/public;
            access_log off;
            autoindex on;
            expires max;
        }

        location ~ ^/(sitemap/|sitemap.xml) {
            root /opt/writersite/public;
            access_log off;
            autoindex on;
            expires 10s;
        }

        location / {
            proxy_redirect off;
            proxy_set_header    X-Real-IP  $remote_addr;
            proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header    X-Forwarded-Proto $scheme;
            proxy_set_header    Host                   $http_host;
            proxy_set_header    X-NginX-Proxy    true;
            proxy_set_header    Connection "";
            proxy_http_version  1.1;
            proxy_cache one;
            proxy_cache_key sfs$request_uri$scheme;
            proxy_pass         http://writersite;
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
}

