var express = require('express');
var passport = require('passport');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');
var MySQLStore = require('connect-mysql')(session);
var env = process.env.NODE_ENV || "dev";

var models = require('./models/index');
var auth = require('./config/auth');
var dbconfig = require('./config/models')["dev"];
var nodeUserGid = "writersite";
var nodeUserUid = "writersite";

// options for the mysql backed session store
var mysqlstoreOpts = {
    config: {
        user: dbconfig.username,
        password: dbconfig.password,
        database: dbconfig.database
    }
};

// begin setting up the express app now
var app = express();

// set up the view engine
app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

// set the app port
app.set('port', process.env.PORT || 3000);

// set up our express application
app.use(morgan('tiny')); // log every request to the console
app.use(cookieParser(auth.cookieSecret)); // read cookies (needed for auth)
app.use(bodyParser.json({limit: '1000kb'})); // get information from html forms
app.use(bodyParser.urlencoded({extended: true, limit: '1000kb'}));

// required for passport
// TODO: look up what resave and saveUnitialized do
app.use(session({
    name: 'sessionId',
    secret: auth.cookieSecret,
    resave: false,
    saveUninitialized: false,
    // we are backing the sessions to a persistent store, in this case it's
    // the mysql database
    store: new MySQLStore(mysqlstoreOpts)
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());            // use connect-flash for flash messages stored in session

// dev only stuff
if (env == "dev") {
    // setup static file handling only in development
    app.use('/', express.static('public'));

    // set json format as pretty printed for development
    app.set('json spaces', 4);
}

// set up passport using passport config module
require('./config/passport')(passport, models.User, models.sequelize);
// add the standard app route
require('./app/routes')(app, passport, models);

// initialize the models before listening for connections.
models.sequelize.sync().then(function () {
    var server = app.listen(app.get('port'), function () {
        if(env != "dev"){
            process.setgid(nodeUserGid);
            process.setuid(nodeUserUid);
        }
        console.log('Express server listening on port ' + server.address().port);
    });
});
