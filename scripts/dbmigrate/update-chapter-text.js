#!/usr/bin/env node
/**
 * Created by hschmale on 5/23/16.
 */

var models = require('../../models/index');
var md = require('marked');

models.Chapter.findAll().then(function(rows){
    rows.forEach(function(chap){
        chap.htmlText = md(chap.text);
        chap.save().then(function(){
            console.log('Updated Chapter id: ', chap.id);
        })
    });
});
