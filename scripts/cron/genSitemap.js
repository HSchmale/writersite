#!/usr/bin/env node
/**
 * Updates the sitemap for the writersite.
 * Created by hschmale on 5/25/16.
 */


var models = require('../../models');
var path = require('path');
var sm = require('sitemap');
var fs = require('fs');

var PROJECT_ROOT = path.dirname(path.dirname(__dirname));

function makeTagsSitemap() {
    var file = path.join(PROJECT_ROOT, 'public/sitemap/tags.xml');
    var sitemap = sm.createSitemap({
        hostname: 'https://www.writersite.xyz'
    });
    models.Tag.findAll({
        where: {
            useCount: {
                $gt: 0
            }
        }
    }).then(function (rows) {
        rows.forEach(function (t) {
            sitemap.add({url: '/tag/' + t.name, changeFreq: 'hourly'});
        });
        console.log('Tags sitemap now has ', rows.length, ' urls');
        fs.writeFileSync(file, sitemap.toString());
    });
}

function makeStorySitemap() {
    var file = path.join(PROJECT_ROOT, 'public/sitemap/stories1.xml');
    var sitemap = sm.createSitemap({
        hostname: 'https://www.writersite.xyz'
    });
    models.Chapter.findAll({
        where: {
        }
    }).then(function (rows) {
        rows.forEach(function (s) {
            sitemap.add({url: '/s/' + s.StoryId + '/' + s.chapterNum, changefreq: 'monthly'});
        });
        console.log('Story Sitemap now has ', rows.length, ' urls');
        fs.writeFileSync(file, sitemap.toString());
    });
}

function makeUserSitemap() {
    var file = path.join(PROJECT_ROOT, 'public/sitemap/users.xml');
    var sitemap = sm.createSitemap({
        hostname: 'https://www.writersite.xyz'
    });
    models.User.findAll().then(function (rows) {
        rows.forEach(function (u) {
            sitemap.add({url: '/u/' + u.id, changefreq: 'monthly'});
        });
        console.log('User Sitemap Created with ', rows.length, ' urls');
        fs.writeFileSync(file, sitemap.toString());
    });
}

function makeNewsSitemap() {
    var file = path.join(PROJECT_ROOT, 'public/sitemap/news.xml');
    var sitemap = sm.createSitemap({
        hostname: 'https://www.writersite.xyz'
    });
    models.NewsItem.findAll().then(function (rows) {
        rows.forEach(function (itm) {
            sitemap.add({
                url: '/news/' + itm.id,
                changefreq: 'yearly'
            });
        });
        console.log('News Sitemap Created with ', rows.length, ' urls');
        fs.writeFileSync(file, sitemap.toString());
    });
}

console.log('Updating Sitemaps');
makeTagsSitemap();
makeStorySitemap();
makeUserSitemap();
makeNewsSitemap();