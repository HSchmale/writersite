#!/usr/bin/python
# Generates the search index for the writersite
import mysql.connector
import re
from sets import Set
from collections import Counter
import sqlite3

MIN_WORD_LENGTH = 3

SQL_GET_REINDEXABLE = """
SELECT id,title
FROM Stories
WHERE Stories.needsReindex = TRUE
"""

SQL_GET_CHAPTER_BY_STORY_ID = """
SELECT id FROM Chapters WHERE StoryId = %s;
"""

SQL_GET_CHAPTER_SAMPLE_BY_ID = """
SELECT
  Chapters.id,
  CONCAT(Stories.title, ' ', IFNULL(Chapters.title, ' '), ' ',
    IFNULL(Stories.description, ' '), ' ', text) as text
FROM Chapters, Stories
WHERE Chapters.StoryId = Stories.id AND Chapters.id = %s;
"""

SQL_FREQ_UPSERT = """
REPLACE INTO FreqIdx(word, StoryId, weight) VALUES
    (%s, %s, %s)
"""

SQL_DELETE_OLD_INDEX = """
DELETE FROM FreqIdx WHERE StoryId = %s;
"""

SQL_UPDATE_STORY_INDEX_STATUS = """
UPDATE Stories SET needsReindex = FALSE WHERE id = %s
"""

def deleteStoryIndexByStoryId(db, StoryId):
    # delete the old index
    print("Deleting Index of Story {}".format(StoryId))
    cursor = db.cursor()
    cursor.execute(SQL_DELETE_OLD_INDEX, (StoryId,))
    cursor.close()
    db.commit()

def indexText(wordDict, text):
    text = re.sub(r"[^A-Za-z' ]", ' ', text).lower()
    words = Set()
    for word in text.split():
        words.add(word)
    for word in words:
        wordDict[word] = wordDict[word] + 1
    return wordDict

def processChapterById(db, chapterId, wordDict):
    cur = db.cursor()
    cur.execute(SQL_GET_CHAPTER_SAMPLE_BY_ID, (chapterId,))
    for (chid, text) in cur:
        wordDict = indexText(wordDict, text)
        print("Chapter: {}, length {}, wordDict {}".format(
            chid, len(text.split()), len(wordDict)))
    cur.close()
    return wordDict

def markStoryAsIndexedById(db, StoryId):
    cur = db.cursor()
    cur.execute(SQL_UPDATE_STORY_INDEX_STATUS, (StoryId,))
    cur.close()
    db.commit()

def processStoryById(db, StoryId):
    deleteStoryIndexByStoryId(db, StoryId)
    # word dict to store the new index temporary
    wordDict = Counter()
    # get the chapters
    cur = db.cursor(buffered=True)
    cur.execute(SQL_GET_CHAPTER_BY_STORY_ID, (StoryId,))
    for (ChapterId,) in cur:
        wordDict = processChapterById(db, ChapterId, wordDict)
    cur.close()
    # replace the index
    for word in wordDict:
        cur = db.cursor()
        cur.execute(SQL_FREQ_UPSERT, (word, StoryId, wordDict[word],))
        cur.close()
    db.commit()
    markStoryAsIndexedById(db, StoryId)


def main():
    # Open Db connection
    db = mysql.connector.connect(
            host="localhost",
            user="writersite",
            passwd="password",
            db="writersite")
    cur = db.cursor(buffered=True)
    cur.execute(SQL_GET_REINDEXABLE)

    for (StoryId, Title) in cur:
        print("processing Story {} - {}".format(StoryId, Title))
        processStoryById(db, StoryId) 

    db.close()

# main sentinel
if __name__ == "__main__":
    main()
