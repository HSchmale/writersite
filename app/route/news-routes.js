/**
 * Applies the routes to fetch the news items
 *
 * Created by hschmale on 5/28/16.
 */

module.exports = function (app, models) {
    app.get('/news', function (req, res) {
        models.NewsItem.findAll({
            order: [['createdAt', 'DESC']],
            limit: 20
        }).then(function (rows) {
            res.render('news-list', {
                user: req.user,
                news: rows
            });
        });
    });

    app.get('/news/:newsid', function(req, res){
        models.NewsItem.findById(req.params.newsid).then(function(news){
            if(!news){
                res.render('404', {
                    user: req.user,
                    url: req.url,
                    msg: 'The news item you are looking for does not exist'
                });
                return;
            }
            res.render('news-display', {
                user: req.user,
                news: news
            })
        });
    });
};