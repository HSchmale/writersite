/**
 * app/routes/ac-routes.js
 *
 * Routes for performing autocompletion of forms.
 *
 * Created by hschmale on 5/19/16.
 */

module.exports = function (app, models) {
    /**
     * Auto completion route
     */
    app.get('/ac/tag', function (req, res) {
        models.Tag.findAll({
            attributes: [
                [models.Sequelize.fn("concat", models.Sequelize.col('Tag.name'), ' - ',
                    models.Sequelize.col('Tag.description')), 'label'],
                ['name', 'value']
            ],
            where: {
                name: {
                    $like: req.query.term + "%"
                }
            }
        }).then(function (tags) {
            res.json(tags);
        });
    });
};