/**
 * Brings in the routes that are split up into separate files
 *
 * Created by hschmale on 5/19/16.
 */

module.exports = function(app, models) {
    require('./browse-routes')(app, models);
    require('./story-routes')(app, models);
    require('./edit-routes')(app, models);
    require('./ac-routes')(app, models);
    require('./pages-routes')(app,models);
    require('./favorite-routes')(app, models);
    require('./user-routes')(app, models);
    require('./news-routes')(app, models);
    require('./search-routes')(app, models);
};