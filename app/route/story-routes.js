/**
 * app/routes/story-routes.js
 *
 * Routes used for viewing stories and the comments related to them
 *
 * Created by hschmale on 5/19/16.
 */

var md = require('marked');

// The exported function
module.exports = function (app, models) {
    /**
     * Redirects for improperly entered story urls
     */
    app.get('/s/:storyid', redirectToStoryBeginning);
    app.get('/s/:storyid/0', redirectToStoryBeginning);

    /**
     * View a story
     */
    app.get('/s/:storyid/:chapnum', function (req, res) {
        var obj = {
            storyid: req.params.storyid,
            chapnum: req.params.chapnum,
            user: req.user,
            url: req.url
        };
        makeStoryPromise(models, obj).then(function (story) {
            if (!story) {
                obj.msg = 'The story or chapter you were looking for was not found.';
                res.status(404).render('404', obj);
                return;
            }
            obj.story = story;
            obj.title = story.title;
            // check if logged in before doing this voodoo.
            // if the user is not logged in this will cause an exception to be thrown, because
            // the user has no id.
            if(req.user) {
                obj.isFavorited = story.StoryFavorites.some(function (e) {
                    return req.user.id == e.UserId
                });
            }
            //res.json(obj);
            res.render('story', obj);
        });
    });

    app.get('/s/:storyid/:chapnum/json', function(req, res){
        makeStoryPromise(models, {
            storyid: req.params.storyid,
            chapnum: req.params.chapnum,
            user: req.user,
            url: req.url
        }).then(function(s){
            res.json(s);
        });
    });

    /**
     * Add a comment to a story
     */
    app.post('/comment', function(req, res){
        if(!req.isAuthenticated()){
            res.redirect('/');
            return;
        }
        if(req.body.comtext.length > 2 && req.body.comtext.length < 4096){
            models.Comment.create({
                UserId: req.user.id,
                StoryId: req.body.storyid,
                text: md(req.body.comtext),
                ChapterNum: parseInt(req.body.chnum, 10)
            }).then(function(c){
                console.log('created comment of id ', c.id);
            });
        }
        res.redirect(req.body.url);
    });
};


/**
 * Route function used to redirect the viewer back to beginning of the
 * story if the url is malformed for their request
 * @param req
 * @param res
 */
function redirectToStoryBeginning(req, res) {
    res.redirect('/s/' + req.params.storyid + '/1');
}

function makeStoryPromise(models, obj){
    return models.Story.find({
        where: {
            id: obj.storyid
        },
        attributes: ['title', 'UserId', 'id', 'chapterCount'],
        include: [
            {
                model: models.Chapter,
                where: {
                    chapterNum: obj.chapnum
                },
                attributes: ['title', ['htmlText', 'text'], 'chapterNum']
            },
            {
                model: models.User,
                attributes: ['displayName']
            },
            {
                model: models.Tag
            },
            {
                model: models.StoryFavorite,
                attributes: ['UserId']
            },
            {
                model: models.Comment,
                include: {
                    model: models.User
                },
                order: [['createdAt', 'DESC']],
                limit: 20
            }
        ]
    })
}