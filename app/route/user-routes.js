/**
 * Contains User Routes
 *
 * Created by hschmale on 5/28/16.
 */

var marked = require('marked');

module.exports = function (app, models) {
    /**
     *  route for showing the profile page
     */
    app.get('/profile', isLoggedIn, function (req, res) {
        var data = {
            md: marked,
            title: req.user.displayName
        };
        models.User.find({
            where: {
                id: req.user.id
            },
            include: [{
                model: models.Story,
                attributes: ['title', 'id', 'description', 'updatedAt'],
                order: ['updatedAt', 'DESC']
            }, {
                model: models.StoryFavorite,
                include: [{
                    model: models.Story,
                    attributes: ['id', 'title', 'description']
                }]
            }]
        }).then(function (user) {
            data.user = user;
            res.render('profile', data);
        });
    });

    app.get('/u/:userid', function (req, res) {
        models.User.find({
            where: {
                id: req.params.userid
            },
            include: [{
                model: models.Story,
                attributes: ['title', 'id', 'description', 'updatedAt'],
                order: ['updatedAt', 'DESC']
            }, {
                model: models.StoryFavorite,
                include: [{
                    model: models.Story,
                    attributes: ['id', 'title', 'description']
                }]
            }]
        }).then(function (user) {
            res.render('user', {
                user: user,
                md: marked,
                title: user.displayName
            });
        });
    });
};

function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/');
}
