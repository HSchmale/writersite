/**
 * app/routes/browse-routes.js
 *
 * Routes used for browsing the collection of stories, or searching it.
 *
 * Created by hschmale on 5/19/16.
 */

var gModels = null;
var config = require('../../config/view');

module.exports = function (app, models) {
    gModels = models;

    /**
     * Redirects to a browse page with numbers
     */
    app.get('/browse', function (req, res) {
        res.redirect('/browse/0');
    });

    /**
     * Browsing the list of stories
     */
    app.get('/browse/:page', function (req, res) {
        var obj = {
            user: req.user,
            list: [],
            title: 'Browse ' + req.params.page
        };
        models.Story.findAll({
            attributes: ['title', 'id', 'description'],
            limit: config.maxBrowsePageResults,
            offset: req.params.page * config.maxBrowsePageResults,
            order: [
                ['id', 'DESC']
            ],
            include: [
                {
                    model: models.User,
                    where: {
                        id: models.sequelize.col('Story.UserId')
                    }
                },
                {
                    model: models.Tag
                }
            ]
        }).then(function (rows) {
            obj.stories = rows;
            res.render('browse', obj);
        });
    });

    /**
     * Browse all of the tags for writing
     */
    app.get('/tag100', function (req, res) {
        models.Tag.findAll({
            attributes: [['name', 'text'], ['useCount', 'weight'], 'description'],
            order: [
                ['useCount', 'desc']
            ],
            where: {
                useCount: {
                    $gt: 0
                }
            },
            limit: 100
        }).then(function (tags) {
            res.render('tag-cloud', {
                user: req.user,
                tags: tags,
                tagJSON: JSON.stringify(tags),
                title: 'Top 100 Tags'
            });
        });
    });

    /**
     * Show all Tags
     */
    app.get('/tag', function (req, res) {
        models.Tag.findAll({
            attributes: ['name', 'useCount', 'description'],
            order: [
                ['name', 'desc']
            ],
            where: {
                useCount: {
                    $gt: 0
                }
            }
        }).then(function (tags) {
            res.render('tag-list', {
                user: req.user,
                tags: tags,
                title: 'All Tags'
            });
        });
    });

    /**
     * Browse stories by Tag Name
     */
    app.get('/tag/:tagname', function (req, res) {
        findTagByName(req.params.tagname).then(function (tag) {
            // TODO: http://stackoverflow.com/q/37355953/4335488
            models.Story.findAll({
                attributes: ['id', 'title', 'description'],
                include: [{
                    model: models.Tag,
                    attributes: ['id'],
                    where: {
                        id: tag.id
                    }
                }, {
                    model: models.User,
                    attributes: ['id', 'displayName'],
                    where: {
                        id: {
                            $col: 'User.id'
                        }
                    }
                }, {
                    model: models.StoryTag,
                    include: [{
                        model: models.Tag,
                        attributes: ['name', 'description']
                    }]
                }]
            }).then(function (stories) {
                res.render('story-by-tag', {
                    user: req.user,
                    tag: tag,
                    stories: stories,
                    title: "Tag: " + req.params.tagname
                });
            });
        });
    });
};

function findTagByName(name) {
    return gModels.Tag.find({
        where: {
            name: name
        },
        order: [
            ['useCount', 'DESC']
        ]
    });
}
