/**
 * app/route/favorite-routes.js
 *
 * Created by hschmale on 5/24/16.
 */


module.exports = function (app, models) {
    app.get('/fav/:storyid/:chapnum', isLoggedIn, function(req, res){
        models.StoryFavorite.create({
            UserId: req.user.id,
            StoryId: req.params.storyid
        });
        res.redirect('/s/' + req.params.storyid + '/' + req.params.chapnum);
    });

    app.get('/ufav/:storyid/:chapnum', isLoggedIn, function(req, res){
        models.StoryFavorite.find({
            where: {
                StoryId: req.params.storyid,
                UserId: req.user.id
            }
        }).then(function(sf){
            return sf.destroy();
        }).then(function(){
            res.redirect('/s/' + req.params.storyid + '/' + req.params.chapnum);
        });
    });
};

/**
 * Checks if a request is authenticated before forwarding them onwards.
 *
 * @param req
 * @param res
 * @param next
 * @returns {*} The next middleware
 */
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/');
}