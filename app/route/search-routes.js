/**
 * Created by hschmale on 6/13/16.
 */

module.exports = function (app, models) {

    /** Returns a sequelize query promise
     *
     * @param qstr The query string
     */
    function generateSearchQuery(qstr) {
        var terms = qstr.trim().toLowerCase().split(/\s+/);
        return models.FreqIdx.findAll({
            attributes: {
                include: [
                    [models.sequelize.fn('SUM', models.sequelize.col('weight')), 'totWeight']
                ],
                exclude: ['word', 'StoryId', 'weight']
            },
            where: {
                word: {
                    $in: terms
                }
            },
            include: [
                {
                    model: models.Story,
                    attributes: {
                        exclude: ['nextChapterNumber']
                    },
                    include: [{
                        model: models.User,
                        attributes: ['displayName', 'imageUrl']
                    }]
                }
            ],
            limit: 20,
            order: 'totWeight DESC',
            group: 'StoryId'
            //,raw: true
        });
    }

    /**
     * The search route
     */
    app.get('/search', function (req, res) {
        generateSearchQuery(req.query.q).then(function (rows) {
            //res.send(rows);
            res.render('search', {
                user: req.user,
                results: rows,
                terms: req.query.q
            })
        });
    });
};

