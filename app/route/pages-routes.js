/**
 * Contains page controller routes
 *
 * Created by hschmale on 5/20/16.
 */

var path = require('path');
var fs = require('fs');

module.exports = function (app) {
    app.get('/pages/:pgnm', function (req, res) {
        var p = path.join(__dirname, '../../views/pages', req.params.pgnm + '.html');
        console.log(p);
        fs.readFile(p, 'utf8', function (err, conn) {
            if (err) {
                console.log(err);
                res.render('404', {
                    user: req.user,
                    title: '404',
                    msg: "The page you were looking for was not found.",
                    url: req.url
                });
                return;
            }

            res.render('pages', {
                user: req.user,
                title: req.params.pgnm,
                content: conn
            });
        });
    });
};
