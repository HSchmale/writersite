/**
 * app/routes/edit-routes.js
 *
 * Contains routes used for editing items by the author. Also includes
 * routes used for creating new entities
 *
 * Created by hschmale on 5/19/16.
 */

var gModels = null;
var md = require('marked');

module.exports = function (app, models) {
    gModels = models;

    /**
     * Display the story edit screen
     */
    app.get('/edit/s/:storyid', checkStoryOwnership, function (req, res) {
        var obj = {
            user: req.user,
            storyId: req.params.storyid,
            title: 'Edit Story'
        };
        models.Story.find({
            where: {
                id: obj.storyId
            },
            include: [
                {
                    model: models.Tag
                }
            ]
        }).then(function (story) {
            if (!story)
                res.render('404', obj);
            obj.storyName = story.title;
            obj.tags = story.Tags;
            obj.storyDescr = story.description;
            res.render('edit_story', obj);
        });
    });

    /**
     * callback route for adding a new tag to a story
     */
    app.post('/edit/s/:storyid/addtag', checkStoryOwnership, function (req, res) {
        findTagByName(req.body.tag).then(function (tag) {
            if (tag) {
                models.StoryTag.create({
                    StoryId: req.params.storyid,
                    TagId: tag.id
                }).then(function () {
                    res.redirect('/edit/s/' + req.params.storyid);
                }).catch(function (err) {
                    res.redirect('/edit/s/' + req.params.storyid);
                    if(err) throw err;
                });
            }
        });
    });

    /**
     * Callback to delete a tag
     */
    app.post('/edit/s/:storyid/deltag', checkStoryOwnership, function (req, res) {
        findTagByName(req.body.tag).then(function (tag) {
            if (tag) {
                models.StoryTag.find({
                    where: {
                        TagId: tag.id,
                        StoryId: req.params.storyid
                    }
                }).then(function (storyTag) {
                    storyTag.destroy();
                    res.redirect('/edit/s/' + req.params.storyid);
                }).catch(function() {
                    res.redirect('/edit/s/' + req.params.storyid);
                })
            }
        });
    });

    app.post('/edit/s/:storyid/title', checkStoryOwnership, function(req, res){
        req.story.title = req.body.title;
        req.story.save().then(function(){
            res.redirect('/edit/s/' + req.params.storyid);
        });
    });
    
    app.post('/edit/s/:storyid/descr', checkStoryOwnership, function (req, res) {
        req.story.description = req.body.descr;
        req.story.save().then(function(){
            res.redirect('/edit/s/' + req.params.storyid);
        });
    });

    /**
     * Display the edit chapter page
     */
    app.get('/edit/s/:storyid/:chapnum', checkStoryOwnership, function (req, res) {
        models.Chapter.find({
            where: {
                StoryId: req.params.storyid,
                chapterNum: req.params.chapnum
            },
            include: [
                {
                    model: models.Story
                }
            ]
        }).then(function (chapter) {
            console.log(chapter.title);
            res.render('edit_chapter', {
                user: req.user,
                story: {id: req.params.storyid},
                chapnum: req.params.chapnum,
                chapter: chapter
            });
        });
    });

    /**
     * Update the chapter with the new text.
     */
    app.post('/edit/s/:storyid/:chapnum', checkStoryOwnership, function (req, res) {
        models.Chapter.find({
            where: {
                StoryId: req.params.storyid,
                chapterNum: req.params.chapnum
            }
        }).then(function (chapter) {
            if (chapter.text !== req.body.text) {
                chapter.text = req.body.text;
                chapter.htmlText = md(chapter.text);
            }
            if (chapter.title !== req.body.title) {
                chapter.title = req.body.title;
            }
            chapter.save().then(function () {
                res.redirect('/s/' + req.story.id + '/' + chapter.chapterNum);
            });
        });
    });


    /**
     * Method to show the create new story screen
     */
    app.get('/new/story', isLoggedIn, function (req, res) {
        var obj = {
            user: req.user,
            title: 'Create Story'
        };
        res.render('new_story', obj);
    });

    /**
     * Creates the new story object with the initial chapter object
     */
    app.post('/new/story', isLoggedIn, function (req, res) {
        models.Story.create({
            title: req.body.title,
            description: req.body.description,
            UserId: req.user.id,
            nextChapterNumber: 2
        }).then(function (story) {
            // filter the tags and enforce uniqueness.
            req.body.tags.filter(function (elem, pos) {
                return req.body.tags.indexOf(elem) == pos;
            }).forEach(function (tag) {
                findTagByName(tag).then(function (t) {
                    if (!t) return;
                    models.StoryTag.create({
                        TagId: t.id,
                        StoryId: story.id
                    });
                });
            });
            // create the first chapter for the new story
            models.Chapter.create({
                title: req.body.chaptitle,
                text: req.body.firstchap,
                htmlText: md(req.body.firstchap),
                StoryId: story.id
            }).then(function () {
                var path = '/s/' + story.id + '/1';
                res.redirect(path);
            }).catch(function () {
                story.destroy();
            });
        });
    });

    /**
     * New Chapter Page
     */
    app.get('/new/chap/:storyid', checkStoryOwnership, function (req, res) {
        res.render('new_chapter', {
            title: "New Chapter",
            user: req.user,
            story: req.story
        });
    });

    /**
     * Called to add new chapter
     */
    app.post('/new/chap/:storyid', checkStoryOwnership, function (req, res) {
        models.Chapter.create({
            title: req.body.title,
            text: req.body.text,
            htmlText: md(req.body.text),
            StoryId: req.story.id,
            chapterNum: req.story.nextChapterNumber
        }).then(function (chapter) {
            req.story.nextChapterNumber++;
            req.story.chapterCount++;
            req.story.save().then(function () {
                res.redirect('/s/' + req.story.id + '/' + chapter.chapterNum);
            });
        });
    });
};

function findTagByName(name) {
    return gModels.Tag.find({
        where: {
            name: name
        },
        order: [
            ['useCount', 'DESC']
        ]
    });
}

/**
 * Checks if a request is authenticated before forwarding them onwards.
 *
 * @param req
 * @param res
 * @param next
 * @returns {*} The next middleware
 */
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/');
}

/**
 * Checks if a story is owned by the user in the specified request.
 *
 * This is a piece of express middleware for certain route.
 *
 * The request must have a storyid param in it's url/
 *
 * @param req
 * @param res
 * @param next
 */
function checkStoryOwnership(req, res, next) {
    if (!req.isAuthenticated()) {
        res.redirect('/signin');
        return;
    }
    gModels.Story.findById(req.params.storyid).then(function (story) {
        if (story) {
            if (req.user.id == story.UserId) {
                req.story = story;
                return next();
            }
        }
        res.redirect('/signin');
    });
}