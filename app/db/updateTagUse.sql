-- displays the total usage count of tags in all scenarios
CREATE VIEW vwFindAllTagUses AS
SELECT TagId, 'StoryTag'
       FROM StoryTags
       UNION ALL
       SELECT TagId, 'UserInterest'
       FROM UserInterests;

-- Sums up the usage of all tags excluding those that are zero
CREATE VIEW vwCountAllTagUsageWithoutZero AS
SELECT
  TagId,
  COUNT(*) as uses
FROM vwFindAllTagUses
GROUP BY TagId;

-- Gets the usage of all tags including those that are zero
CREATE VIEW vwGetUsageOfAllTags AS
SELECT TagId, uses FROM vwCountAllTagUsageWithoutZero
UNION
-- Find the tags that have zero use
SELECT id, 0 as 'uses'
FROM Tags
WHERE
	id NOT IN (SELECT TagId FROM vwCountAllTagUsageWithoutZero);

DELIMITER $$
-- Gives a rough estimate for Tags.useCount with the use of story tags
-- Tag.useCount is updated more precisely once every 4 hours in
-- production with a procedure
CREATE TRIGGER updateTagUse AFTER INSERT ON StoryTags
FOR EACH ROW BEGIN
  UPDATE Tags
  SET useCount = useCount + 1
  WHERE
    id = NEW.TagId;
END; $$

CREATE PROCEDURE updateTagsUsageCount()
  BEGIN
    DECLARE $tagId INTEGER DEFAULT 0;
    DECLARE $useCount INTEGER DEFAULT 0;
    DECLARE $done INTEGER DEFAULT 0;
    DECLARE $cur CURSOR FOR SELECT
                              TagId,
                              uses
                            FROM vwGetUsageOfAllTags, Tags as t
                            WHERE t.id = vwGetUsageOfAllTags.TagId
                            AND uses != useCount;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET $done = 1;

    OPEN $cur;
    read_loop: LOOP
      FETCH $cur
      INTO $tagId, $useCount;
      IF $done
      THEN
        LEAVE read_loop;
      END IF;

      UPDATE Tags
      SET useCount = $useCount
      WHERE id = $tagId;

    END LOOP read_loop;
    CLOSE $cur;
  END; $$

CREATE EVENT evUpdateTagUseCount ON SCHEDULE EVERY 1 HOUR DO CALL updateTagsUsageCount()
DELIMITER ;
