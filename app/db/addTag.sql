DELIMITER $$
CREATE PROCEDURE addTag(n VARCHAR(255), d VARCHAR(255))
  BEGIN
    INSERT INTO Tags(name, description) VALUES
      (n, d);
  END; $$
DELIMITER ;

