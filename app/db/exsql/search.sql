-- contains example sql for the writer site search engine

-- query to get stories that need to be reindexed
SELECT
    Stories.id as StoryId,
    Chapters.chapterNum AS chapNum,
    Users.displayName as author,
    Chapters.StoryId as StoryId,
    CONCAT(Stories.title, ' - ', IFNULL(Chapters.title, ' '))
FROM Chapters, Stories, Users
WHERE Chapters.StoryId = Stories.id AND
    Users.id = Stories.UserId AND
    Stories.needsReindex = TRUE
ORDER BY Chapters.StoryId, Chapters.chapterNum;


-- This is the search query used to find stories matching the given words
SELECT
  GROUP_CONCAT(word),
  title,
  StoryId,
  SUM(weight) AS totWeight,
  count(*),
  count(*) / SUM(weight) as relv
FROM FreqIdx, Stories
WHERE
	word IN ('the', 'with', 'magic') AND
    FreqIdx.StoryId = Stories.id
GROUP BY storyid
ORDER BY relv ASC, totWeight DESC;

-- insert statement for properly updating values via insert
INSERT INTO freq(word, storyid, freq) VALUES
    (%s, %s, 1)
    ON DUPLICATE KEY UPDATE freq=freq+1;