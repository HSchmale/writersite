DELIMITER $$

-- when a new chapter is added
-- notify all who have favorited the story.
CREATE TRIGGER newChapterNotification AFTER INSERT ON Chapters
FOR EACH ROW
  BEGIN
    DECLARE $done INTEGER DEFAULT 0;
    DECLARE $storyName VARCHAR(255);
    DECLARE $msg VARCHAR(512);
    -- id of user who is watching this story
    DECLARE $watcherId INTEGER DEFAULT 0;
    -- query to find all users who are watching for updates on a particular
    -- story
    DECLARE $cur CURSOR FOR SELECT UserId
                            FROM StoryFavorites
                            WHERE StoryId = NEW.StoryId;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET $done = 1;

    SELECT title
    INTO $storyName
    FROM Stories
    WHERE id = NEW.StoryId;

    SELECT CONCAT('New Chapter posted for <a href="/s/', NEW.StoryId, '/',
                  NEW.chapterNum, '">', $storyName, '</a>') INTO $msg;

    OPEN $cur;
    read_loop: LOOP
      FETCH $cur
      INTO $watcherId;
      IF $done
      THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO Notifications (type, msg, ChapterId, OwnerId, StoryId, createdAt) VALUES
        (0, $msg, NEW.id, $watcherId, NEW.StoryId, NEW.createdAt);
    END LOOP read_loop;
  END; $$

-- trigger to notify on an update to a chapter from a story a user has
-- favorited. pretty much the same as the above with a few key differences
-- like using the update time as the notification sent time.
CREATE TRIGGER updatedChapterNotification AFTER UPDATE ON Chapters
FOR EACH ROW
  BEGIN
    DECLARE $done INTEGER DEFAULT 0;
    DECLARE $storyName VARCHAR(255);
    DECLARE $msg VARCHAR(512);
    -- id of user who is watching this story
    DECLARE $watcherId INTEGER DEFAULT 0;
    -- query to find all users who are watching for updates on a particular
    -- story
    DECLARE $cur CURSOR FOR SELECT UserId
                            FROM StoryFavorites
                            WHERE StoryId = NEW.StoryId;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET $done = 1;

    SELECT title
    INTO $storyName
    FROM Stories
    WHERE id = NEW.StoryId;

    SELECT CONCAT('Chapter updated for <a href="/s/', NEW.StoryId, '/',
                  NEW.chapterNum, '">', $storyName, '</a>') INTO $msg;

    OPEN $cur;
    read_loop: LOOP
      FETCH $cur
      INTO $watcherId;
      IF $done
      THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO Notifications (type, msg, ChapterId, OwnerId, StoryId, createdAt) VALUES
        (0, $msg, NEW.id, $watcherId, NEW.StoryId, NEW.updatedAt);
    END LOOP read_loop;
  END; $$

-- trigger allows authors to be notified of a new comment on their story.
CREATE TRIGGER authorCommentNotify AFTER INSERT ON Comments
FOR EACH ROW BEGIN
  DECLARE $done INTEGER DEFAULT 0;
  DECLARE $authorId INTEGER DEFAULT 0;
  DECLARE $storyName VARCHAR(255);
  DECLARE $msg VARCHAR(512);
  -- id of user who is watching this story
  DECLARE $watcherId INTEGER DEFAULT 0;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET $done = 1;

  SELECT UserId,title INTO $authorId,$storyName FROM Stories WHERE NEW.StoryId = id;

  SELECT CONCAT('New <a href="/s/', NEW.StoryId, '/', NEW.ChapterNum, '#c', NEW.id,
                '">comment</a> on <a href="/s/', NEW.StoryId, '">', $storyName, '</a>')
    INTO $msg;

  INSERT INTO Notifications(type, msg, ChapterId, OwnerId, StoryId, createdAt) VALUES
    (0, $msg, NULL, $authorId, NEW.StoryId, NOW());
END; $$


DELIMITER ;