DELIMITER $$
DROP TRIGGER IF EXISTS MarkStoryReindexOnStoryUpdate;
CREATE TRIGGER MarkStoryReindexOnChapterUpdate BEFORE UPDATE ON Chapters
  FOR EACH ROW BEGIN
    -- any update to any chapter invalidates the index for the whole story
    -- we could just periodically flush the story index, but this way is
    -- better.
    IF new.text != old.text OR new.title != old.title THEN
      UPDATE Stories SET needsReindex = TRUE WHERE id = NEW.StoryId;
    END IF;
END; $$

DROP TRIGGER IF EXISTS MarkStoryReindexOnStoryUpdate;
CREATE TRIGGER MarkStoryReindexOnStoryUpdate BEFORE UPDATE ON Stories
FOR EACH ROW BEGIN
  IF new.title != old.title OR new.description != old.description THEN
    SET new.needsReindex = TRUE;
  END IF;
END; $$

DROP TRIGGER IF EXISTS MarkStoryReindexOnChapterInsert;
CREATE TRIGGER MarkStoryReindexOnChapterInsert AFTER INSERT ON Chapters
FOR EACH ROW BEGIN
  UPDATE Stories SET needsReindex = TRUE WHERE id = NEW.StoryId;
END; $$

DELIMITER ;