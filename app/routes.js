/**
 * app/routes.js
 * Henry J Schmale
 *
 * Defines standard application routes, and brings in other routes by
 * explicitly including them from the app/routes folder.
 *
 */
// Markdown engine
var gModels = null;
var gRecentNews = [];
var gPopularTags = [];

module.exports = function (app, passport, models) {
    // make models available globally to this module
    gModels = models;

    // fetch initial cache data
    updateRecentNewsItems();
    updateTopTags();

    // set an update rate for the cached feed items
    // every 15 minutes, because the news should be updated fairly frequently.
    setInterval(updateRecentNewsItems, 1000 * 60 * 15);
    // Every 1hr 1mn 40s
    // To allow time for the database updater to fire
    setInterval(updateTopTags, 1000 * 3700);


    // route for home page
    // It changes depending on if the user is logged in
    app.get('/', function (req, res) {
        if (!req.user) {
            // user is not defined, so not logged in
            return res.render('index-nologin', {
                news: gRecentNews,
                tags: gPopularTags
            });
        }
        models.Notification.findAll({
            limit: 10,
            attributes: ['msg', 'createdAt'],
            where: {
                OwnerId: req.user.id
            },
            order: [
                ['createdAt', 'DESC']
            ]
        }).then(function (notes) {
            res.render('index-loggedin', {
                user: req.user,
                news: gRecentNews,
                tags: gPopularTags,
                notifies: notes
            })
        });
    });

    /**
     * Display the signin page
     */
    app.get('/signin', function (req, res) {
        res.render('signin', {title: 'Sign In'});
    });


    // route for logging out the current user
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    // Bring in the additional routes from the modules
    require('./route/index')(app, models);

    // =====================================
    // GOOGLE ROUTES
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google',
        passport.authenticate('google', {scope: ['profile', 'email']}));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback', passport.authenticate('google', {
        successRedirect: '/profile',
        failureRedirect: '/'
    }));
};

/**
 * Updates the most recent news item array with the latest items from the
 * database.
 */
function updateRecentNewsItems() {
    gModels.NewsItem.findAll({
        limit: 3,
        order: [
            ['createdAt', 'DESC']
        ]
    }).then(function (news) {
        gRecentNews = news;
    });
}

/**
 * Updates the most popular tags array with the latest ones
 */
function updateTopTags() {
    gModels.Tag.findAll({
        where: {
            useCount: {
                $gt: 0
            }
        },
        order: [['useCount', 'DESC']],
        limit: 5
    }).then(function (tags) {
        gPopularTags = tags;
    })
}