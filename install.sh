#!/usr/bin/env bash

PROJ_ROOT=$(pwd)

function replaceIfDiff() {
    if ! cmp $1 $2 > /dev/null 2>&1
    then
        echo replacing file: $2
        cp $1 $2
    fi
}

function getFileName() {
    local f=$(basename "$1")
    echo ${f%.*}
}

function generateHtmlFromMd() {
    local out=$(dirname $1)/$(getFileName $1).html
    echo Generating $out
    markdown_py $1 > $out
}

case "$1" in
    update)
        git reset --hard
        git pull
    ;;
esac


# generate static pages
cd views/pages
for f in *.md
do
    generateHtmlFromMd $f
done
cd $PROJ_ROOT

# migrate the database
sequelize db:migrate

# only on the production instance
if [ "$(hostname)" = 'mysql' ]
then
    # update npm modules
    npm install

    # update the system configuration files
    replaceIfDiff sysconf/nginx.conf /etc/nginx/nginx.conf
    replaceIfDiff sysconf/writer-site /etc/init.d/writer-site
fi

echo "Now Restart The Node Process"
