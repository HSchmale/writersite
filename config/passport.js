/**
 * config/passport.js
 *
 * Provides configuration of the passport system
 *
 *
 */

// load all the things we need
//var LocalStrategy    = require('passport-local').Strategy;
//var FacebookStrategy = require('passport-facebook').Strategy;
//var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// load the auth variables
var configAuth = require('./auth');

/**
 * passport: The passport object used to authenicate
 * User: The user sequelize object
 */
module.exports = function (passport, User, sequelize) {
    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id).then(function (user) {
            done(null, user);
        }).catch(function (err) {
            if (err) throw err;
        });
    });

    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))
    // code for facebook (use('facebook', new FacebookStrategy))
    // code for twitter (use('twitter', new TwitterStrategy))

    // =========================================================================
    // GOOGLE
    // =========================================================================
    passport.use(new GoogleStrategy({
        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL
    }, function (req, token, refreshToken, profile, done) {
        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function () {
            // try to find the user based on their google id
            if (!req.user) {
                User.findOne({
                    attributes: ['id'],
                    where: {
                        oauthUserId: profile.id,
                        oauthProvider: 'google'
                    }
                }).then(function (user) {
                    if (user) {
                        if (!user.oauthToken) {
                            user.oauthToken = token;
                            user.email = profile.emails[0].value;
                            user.lastLoggedIn = sequelize.fn('NOW');
                            user.imageUrl = profile.photos[0].value;

                            user.save().then(function (user) {
                                return done(null, user);
                            })
                        } else {
                            return done(null, user);
                        }

                    } else {
                        // if the user is not in our database, create a new user
                        User.create({
                            oauthUserId: profile.id,
                            oauthProvider: 'google',
                            oauthToken: token,
                            displayName: profile.displayName,
                            realName: profile.displayName,
                            email: profile.emails[0].value,
                            imageUrl: profile.photos[0].value
                        }).then(function (newUser) {
                            return done(null, newUser);
                        }).catch(function (err) {
                            if (err) throw err;
                        });
                    }
                });
            }
        });

    }));
};