/**
 * config/view.js
 *
 * Configures options for the view. Like how many results per page.
 *
 * Created by hschmale on 5/20/16.
 */

module.exports = {
    maxBrowsePageResults: 20
};