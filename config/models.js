/**
 * Configuration for the models, and database access.
 *
 * @type {{username: string, database: string, password: string, dialect: string, host: string}}
 */

module.exports = {
    dev: {
        username: 'writersite',
        database: 'writersite',
        password: 'password',
        dialect: 'mysql',
        host: 'localhost',
        //logging: false
    }
};