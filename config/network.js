/**
 * Created by hschmale on 5/20/16.
 */

var env = process.env.NODE_ENV || "dev";
var obj = {
    hostname: 'http://test.henryschmale.org'
};
module.exports = obj;