# Help
This page is meant to offer some general help to users, if this does not
help, you may contact us via email or using the [contact][2] page

# Editing Stories
All stories are written in [markdown][1]. This is an easy to use markup
language that looks just like any other formatted plaintext. Paragraphs
are separated from each other by two newlines. Headings are prefixed 
with up to six hashes(#) to denote the level. Lists are just asterisks.

An example of markdown looks like the following:


    # A Heading
    paragraph 1

    paragraph 2.
    still paragraph 2.

    * I am part of an unordered list
    * I am bullet 2 in the same list

    ## Heading Level 2 - I am smaller than the first heading
    1. Ordered List Item 1
    2. Ordered list item 2
    3. Ordered list item 3
    
        I also support monospaced text. Anything indented by 4 spaces
        will be formatted as it is layed out in the text box. You must
        have a blank line above and below me for this to work.
        
    You can also add links in this format. An inline link is shown 
    [here](http://google.com). You can also do reference links like
    [so][1].
    
    You can also format text to be **bold** or _italic_, by using the
    previous syntax. You can also do `monospace` text inline by using
    backticks.
    
    > You can even do block quotes. I am an example of that. Just
    > remember to begin each line with a `>`
    
    The below is a reference link. However you don't have to be limited
    to just numbers for these. They can be anykind of text you like.
    
    [1]: http://google.com

A markdown tutorial is available [here][md_tut].

# Tagging Your Stories
This section is meant to help you with understanding how to tag your
stories. To apply tags to one of your stories take the following steps:

1. Go to your story.
2. Click the edit story link on the side bar
3. Go to the add tags section
4. You can type in tags into the bar, and it will autocomplete them for
   you.


[1]: https://daringfireball.net/projects/markdown/
[2]: /pages/contact
[md_tut]: https://daringfireball.net/projects/markdown/syntax