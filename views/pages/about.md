# About
This is a site of writers of all sorts to collaborate, and share their 
work. Whether that work is original or fanfiction it is welcome here.
It is meant to be an open platform for sharing works of all kinds.

However, this site is not just for writers. It is also for readers, with
powerful notifications for many events. They can watch stories, authors,
or even whole tags for changes.