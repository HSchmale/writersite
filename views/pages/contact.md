# Contact Us
This page allows you to contact us.

<form action="https://formspree.io/help@writersite.xyz" method=post>
    <div class="form-group">
        <label for="name">Your Name</label>
        <input type="text" name="name" class="form-control" id="name">
    </div>
    <div class="form-group">
        <label for="contactMethod">How to Contact You</label>
        <input type="text" name="contactMethod" class="form-control" 
            id="contactMethod">
    </div>
    <div class="form-group">
        <label for="message">Your Message</label>
        <textarea class="form-control" name="message" rows=5 
            id="message"></textarea>
    </div>
    <div class="form-group">
        <input type="submit">
</form>