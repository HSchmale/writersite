# Rules
These are just some general guidelines when it comes to stories, and
what is appropriate to post.

1. Mature content **must** be tagged 'mature'. This is to provide fair
   warning to people who might not like that kind of stuff.
2. Don't post things that don't belong to you. Fanfiction is completely
   acceptable, just don't post complete copyrighted works here, they
   will be deleted. Remember to cite things that don't belong to you.
3. Don't be hateful. Criticizing works is completely acceptable, but
   be polite about it. Don't use ad hominim attacks, as that shows that
   you are weak minded.

More rules will be added here as the need arises.