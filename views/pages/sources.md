# Sources
Writersite uses the following libraries.

* [jquery][1]
* [jquery-ui][2]
* [Bootstrap 3][3]
* [MarkItUp! Universal Text Editor][4]
* [Marked.js][5]
* [Bootstrap Tag Cloud][6]
* [JQcloud][7]
* [Sequelize][8]
* [Jade][9]
* [ExpressJS][10] and associated middlewares
* [PassportJS][11]

This site is powered by:



[1]: https://jquery.com/
[2]: https://jqueryui.com/
[3]: http://getbootstrap.com/
[4]: http://markitup.jaysalvat.com/home/
[5]: https://github.com/chjj/marked
[6]: http://collectivepush.com/plugins/bootstrap/
[7]: http://mistic100.github.io/jQCloud/
[8]: https://github.com/sequelize/sequelize
[9]: http://jade-lang.com/
[10]: http://expressjs.com/
[11]: http://passportjs.org/