/**
 * Created by hschmale on 5/18/16.
 */

$('#tagAdder').autocomplete({
    source: '/ac/tag',
    minLength: 1
});