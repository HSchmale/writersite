# Writer Site
A simple site for writers of all sorts to collaborate, and share their
work. It uses tags to store all of the information.

This site used to be available at https://www.writersite.xyz, but I shut it down.
This is now an opensource project available to anyone who wants the code.

# Installation and Updates
1. Clone this project into `/opt/writersite`
2. cd to that directory
3. Run `install.sh`
4. Make sure that nginx and mysql are available.
5. Start them before spinning this up. Start this server by running
    `systemctl start writer-site`

To update this program just run `./install.sh update` from the project root
on the server. Then you can restart this program. **Make sure you did not make
any changes to the production one first otherwise it will fail**

# Project Structure
Project is structured as a standard nodejs package. The main file is
`server.js`. It sets up the express app.

`app` contains various library files, and the routes.

`app/cron` contains things for setting up the app cron jobs.

`app/db` contains the sql procedures and views for the application.

`app/route` defines the application routes.

`config` contains application configuration settings, defined as node modules.

`sysconf` global configuration for the server. Things that belong `/etc`.

`public` contains static assets, and in production will be served by nginx
instead of node.

`views` contains the jade templates to generate the various application
views.

`views/pages` contains the markdown used for the pages controller. This
markdown is converted to html on installation on the server.