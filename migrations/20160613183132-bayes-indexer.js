'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        /*
         Add altering commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        return [
            queryInterface.addColumn('Stories', 'needsReindex', {
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }),
            queryInterface.createTable('FreqIdx', {
                word: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                weight: {
                    type: Sequelize.INTEGER,
                    defaultValue: 0
                },
                StoryId: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'Stories',
                        key: 'id'
                    },
                    onUpdate: 'cascade',
                    onDelete: 'cascade'
                }
            }),
            queryInterface.addIndex("FreqIdx", ['word', 'StoryId'], {
                indicesType: "UNIQUE"
            })
        ];
    },

    down: function (queryInterface) {
        /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         */
        return [
            queryInterface.removeColumn('Stories', 'needsReindex'),
            queryInterface.dropTable('FreqIdx')
        ];
    }
};
