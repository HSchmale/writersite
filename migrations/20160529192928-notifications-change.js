/**
 * Change to how the notifications are modeled
 */

'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        /*
         Add altering commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        return [
            queryInterface.renameColumn('Notifications', 'UserId', 'OwnerId'),
            queryInterface.addColumn('Notifications', 'chapNum', {
                type: Sequelize.INTEGER
            }),
            queryInterface.addColumn('Notifications', 'msg', {
                type: Sequelize.STRING(512)
            })
        ];
    },

    down: function (queryInterface, Sequelize) {
        /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
    }
};
