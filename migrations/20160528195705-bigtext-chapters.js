'use strict';

module.exports = {
    up: function (queryInterface) {
        /*
         Add altering commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        return [
            queryInterface.changeColumn(
                'Chapters',
                'text',
                {
                    type: 'LONGTEXT',
                    allowNull: false
                }),
            queryInterface.changeColumn(
                'Chapters',
                'htmlText',
                {
                    type: "LONGTEXT",
                    allowNull: false
                }
            )
        ];
    },

    down: function (queryInterface, Sequelize) {
        /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
    }
};
