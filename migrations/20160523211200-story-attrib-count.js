'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        /*
         Add altering commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        return [
            queryInterface.addColumn('Stories', 'chapterCount', {
                type: Sequelize.INTEGER,
                defaultValue: 1
            }),
            queryInterface.addColumn('Stories', 'estimatedWordCount', {
                type: Sequelize.INTEGER
            }),
            queryInterface.addColumn('Chapters', 'estimatedWordCount', {
                type: Sequelize.INTEGER
            }),
            queryInterface.addColumn('Chapters', 'htmlText', {
                type: Sequelize.TEXT
            })
        ];
    },

    down: function (queryInterface, Sequelize) {
        /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
        return [
            queryInterface.removeColumn('Stories', 'chapterCount'),
            queryInterface.removeColumn('Stories', 'estimatedWordCount'),
            queryInterface.removeColumn('Chapters', 'estimatedWordCount'),
            queryInterface.removeColumn('Chapters', 'htmlText')
        ];
    }
};
