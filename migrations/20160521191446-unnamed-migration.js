'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        /*
         Add altering commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        return [
            queryInterface.addColumn('Users', 'isAdmin', {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            }),
            queryInterface.addColumn('Users', 'isMod', {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            }),
            queryInterface.addColumn('Users', 'isBanned', {
                type: Sequelize.BOOLEAN,
                defaultValue: false
            })
        ]
    },

    down: function (queryInterface, Sequelize) {
        /*
         Add reverting commands here.
         Return a promise to correctly handle asynchronicity.

         Example:
         return queryInterface.dropTable('users');
         */
        return [
            queryInterface.removeColumn('Users', 'isAdmin'),
            queryInterface.removeColumn('Users', 'isMod'),
            queryInterface.removeColumn('Users', 'isBanned')
        ]
    }
};
